/* 4.1 БД «Комп. фірма». Знайдіть виробників, що випускають ПК,
але не ноутбуки (використати операцію IN). Вивести maker.*/
SELECT 
    maker
FROM
    (SELECT 
        maker
    FROM
        product
    WHERE
        type = 'PC'
    GROUP BY maker) AS MakerPC
WHERE
    MakerPC.maker NOT IN (SELECT 
            maker
        FROM
            product
        WHERE
            type = 'Laptop'
        GROUP BY maker);

/* 4.2 БД «Комп. фірма». Знайдіть виробників, що випускають ПК,
але не ноутбуки (використати ключове слово ALL). Вивести maker.*/
SELECT 
    maker
FROM
    product
WHERE
    maker <> ALL (SELECT 
            maker
        FROM
            product
        WHERE
            type = 'Laptop'
        GROUP BY maker)
        AND type = 'PC'
GROUP BY maker;

/* 4.3 БД «Комп. фірма». Знайдіть виробників, що випускають ПК,
але не ноутбуки (використати ключове слово ANY). Вивести maker.*/
SELECT 
    maker
FROM
    product
WHERE
    maker != ANY (SELECT 
            maker
        FROM
            product
        WHERE
            type = 'PC'
        GROUP BY maker)
        AND type = 'Printer'
GROUP BY maker
HAVING COUNT(type) = 1;

/* 4.4 БД «Комп. фірма». Знайдіть виробників, що випускають
одночасно ПК та ноутбуки (використати операцію IN).
Вивести maker.*/
SELECT 
    maker
FROM
    (SELECT 
        maker
    FROM
        product
    WHERE
        type = 'PC'
    GROUP BY maker) AS MakerPC
WHERE
    MakerPC.maker IN (SELECT 
            maker
        FROM
            product
        WHERE
            type = 'Laptop'
        GROUP BY maker);

/* 4.5 БД «Комп. фірма». Знайдіть виробників, що випускають
одночасно ПК та ноутбуки (використати ключове слово ALL).
Вивести maker.*/
SELECT 
    maker
FROM
    product
WHERE
    NOT maker <> ALL (SELECT 
            maker
        FROM
            product
        WHERE
            type = 'Laptop'
        GROUP BY maker)
        AND type = 'PC'
GROUP BY maker;

/* 4.6 БД «Комп. фірма». Знайдіть виробників, що випускають
одночасно ПК та ноутбуки (використати ключове слово ANY).
Вивести maker.*/
SELECT 
    maker
FROM
    product
WHERE
    maker = ANY (SELECT 
            maker
        FROM
            product
        WHERE
            type = 'Laptop'
        GROUP BY maker)
        AND type = 'PC'
GROUP BY maker;

/* 4.7 БД «Комп. фірма». Знайти тих виробників ПК, усі моделі ПК
яких є у наявності в таблиці PC (використовувати вкладені
підзапити та оператори IN, ALL, ANY). Вивести maker.*/
SELECT 
    maker
FROM
    pc
        JOIN
    product ON pc.model = product.model
WHERE
    product.model IN (SELECT DISTINCT
            model
        FROM
            pc)
GROUP BY maker;

/* 4.8 БД «Кораблі». Вивести класи усіх кораблів України ('Ukraine').
Якщо у БД немає класів кораблів України, тоді вивести класи
для усіх наявних у БД країн. Вивести: country, class.*/
SELECT 
    classes.class, country
FROM
    classes
WHERE
    class = ALL (SELECT 
            classes.class
        FROM
            classes
                JOIN
            ships ON classes.class = ships.class
        WHERE
            country = 'Ukraine');
   
/* 4.9 БД «Кораблі». Знайдіть кораблі, «збережені для майбутніх
битв», тобто такі, що були виведені з ладу в одній битві
('damaged'), а потім (пізніше у часі) знову брали участь у
битвах. Вивести: ship, battle, date.*/
SELECT 
    ship, battle, date
FROM
    outcomes
        JOIN
    battles ON outcomes.battle = battles.name
WHERE
    ship IN (SELECT 
            ship
        FROM
            outcomes
        WHERE
            result = 'damaged' OR result = 'OK'
        GROUP BY ship
        HAVING COUNT(battle) = 2);

/* 4.10 БД «Комп. фірма». Знайти виробників ПК, моделей яких немає
у продажу (тобто відсутні у таблиці PC).*/
SELECT DISTINCT
    maker
FROM
    pc
        RIGHT JOIN
    product ON product.model = pc.model
WHERE
    product.type = 'PC'
        AND product.model NOT IN (SELECT DISTINCT
            model
        FROM
            pc);

/* 4.11 БД «Комп. фірма». Знайти наявну кількість комп’ютерів, що
випущені виробником 'A'.*/
SELECT 
    *
FROM
    pc
WHERE
    model IN (SELECT 
            model
        FROM
            product
        WHERE
            maker = 'A' AND type = 'PC');

/* 4.12 БД «Комп. фірма». Знайти моделі та ціни ноутбуків, вартість
яких є вищою за вартість будь-якого ПК. Вивести: model, price.*/
SELECT 
    model, price
FROM
    laptop
WHERE
    price > (SELECT 
            MAX(price)
        FROM
            pc);

/* 5.1 БД «Комп. фірма». Знайти тих виробників ПК, усі моделі ПК
яких є у наявності в таблиці PC (використовуючи операцію
EXISTS). Вивести maker.*/
SELECT DISTINCT
    maker
FROM
    pc
        JOIN
    product ON pc.model = product.model
WHERE
    product.model IN (SELECT DISTINCT
            model
        FROM
            pc)
        AND EXISTS( SELECT 
            product.model
        FROM
            pc
                RIGHT JOIN
            product ON product.model = pc.model
        WHERE
            type = 'PC' AND pc.model IS NULL);

/* 5.2 БД «Комп. фірма». Знайдіть виробників, які б випускали ПК зі
швидкістю 750 МГц та вище. Виведіть: maker.*/
SELECT DISTINCT
    maker
FROM
    product
WHERE
    type = 'PC'
        AND model IN (SELECT DISTINCT
            model
        FROM
            pc
        WHERE
            speed >= 750);

/* 5.3 БД «Комп. фірма». Знайдіть виробників, які б випускали
одночасно ПК та ноутбуки зі швидкістю 750 МГц та вище. Виведіть: maker.*/
SELECT 
    maker
FROM
    (SELECT 
        maker
    FROM
        product
    JOIN pc ON product.model = pc.model
    WHERE
        type = 'PC' AND speed >= 750
    GROUP BY maker) AS MakerPC
WHERE
    MakerPC.maker IN (SELECT 
            maker
        FROM
            product
                JOIN
            laptop ON product.model = laptop.model
        WHERE
            type = 'Laptop' AND speed >= 750
        GROUP BY maker);

/* 5.4 БД «Комп. фірма». Знайдіть виробників принтерів, що
випускають ПК з найвищою швидкістю процесора. Виведіть:maker.*/
SELECT DISTINCT
    maker
FROM
    product
WHERE
    type = 'Printer'
        AND maker IN (SELECT 
            maker
        FROM
            product
                JOIN
            pc ON product.model = pc.model
        WHERE
            speed = (SELECT 
                    MAX(speed)
                FROM
                    pc));

/* 5.5 За Вашингтонським міжнародним договором від початку 1922
р. заборонялося будувати лінійні кораблі водотоннажністю
понад 35 тис. тонн. Вкажіть кораблі, що порушили цей договір
(враховувати лише кораблі з відомим спуском на воду, тобто з
таблиці Ships). Виведіть: name, launched, displacement.*/
SELECT 
    name, launched, displacement
FROM
    ships
        JOIN
    classes ON ships.class = classes.class
WHERE
    displacement >= 35000
        AND name IN (SELECT 
            name
        FROM
            ships
        WHERE
            launched >= 1922)
ORDER BY launched;

/* 5.6 БД «Кораблі». Знайдіть класи кораблів, у яких хоча б один
корабель був затоплений у битвах. Вивести: class. (Назви
класів кораблів визначати за таблицею Ships, якщо його там
немає, тоді порівнювати чи його назва не співпадає з назвою
класу, тобто він є головним)*/
SELECT 
    classes.class
FROM
    ships
        JOIN
    classes ON ships.class = classes.class
WHERE
    name IN (SELECT 
            ship
        FROM
            outcomes
        WHERE
            result = 'sunk');

/* 5.7 БД «Комп. фірма». Виведіть тих виробників ноутбуків, які
також випускають і принтери.*/
SELECT 
    maker
FROM
    (SELECT 
        maker
    FROM
        product
    WHERE
        type = 'Laptop'
    GROUP BY maker) AS MakerPC
WHERE
    MakerPC.maker IN (SELECT 
            maker
        FROM
            product
        WHERE
            type = 'Printer'
        GROUP BY maker);

/* 5.8 БД «Комп. фірма». Виведіть тих виробників ноутбуків, які не
випускають принтери.*/
SELECT 
    maker
FROM
    (SELECT 
        maker
    FROM
        product
    WHERE
        type = 'Laptop'
    GROUP BY maker) AS MakerPC
WHERE
    MakerPC.maker NOT IN (SELECT 
            maker
        FROM
            product
        WHERE
            type = 'Printer'
        GROUP BY maker);

/* 6.1 БД «Комп. фірма». Виведіть середню ціну ноутбуків з
попереднім текстом 'середня ціна = '.*/
SELECT 
    CONCAT('середня ціна = ',
            CAST(AVG(price) AS CHAR)) AS 'Ціна'
FROM
    laptop;

/* 6.2 БД «Комп. фірма». Для таблиці PC вивести усю інформацію з
коментарями у кожній комірці, наприклад, 'модель: 1121',
'ціна: 600,00' і т.д.*/
SELECT 
    CONCAT('модель: ', model) Model,
    CONCAT('швидкість: ', speed) Speed,
    CONCAT('об’єм пам’яті: ',
            ram,
            ' мб') RAM,
    CONCAT('розмір диску: ', hd, ' Гб') HD,
    CONCAT('швидкість CD-приводу: ',
            cd) CD,
    CONCAT('ціна: ', price) Price
FROM
    pc;

/* 6.3 БД «Фірма прий. вторсировини». З таблиці Income виведіть
дати у такому форматі: рік.число_місяця.день, наприклад,
2001.02.15 (без формату часу).*/
SELECT 
    CONCAT(YEAR(date),
            '.',
            MONTH(date),
            '.',
            DAY(date)) AS DATE
FROM
    income;

/* 6.4 БД «Кораблі». Для таблиці Outcomes виведіть дані, а заміть
значень стовпця result, виведіть еквівалентні їм надписи
українською мовою.*/
SELECT 
    ship,
    battle,
    CASE
        WHEN result = 'OK' THEN 'Справний'
        WHEN result = 'damaged' THEN 'Пошкоджений'
        WHEN result = 'sunk' THEN 'Затоплений'
        ELSE 'Невідомо'
    END AS result
FROM
    outcomes;

/* 6.5 БД «Аеропорт». Для таблиці Pass_in_trip значення стовпця
place розбити на два стовпця з коментарями, наприклад,
перший – 'ряд: 2' та другий – 'місце: a'.*/
SELECT 
    trip_no,
    date,
    ID_psg,
    CONCAT('ряд: ', LEFT(place, 1)) 'row',
    CONCAT('місце: ', RIGHT(place, 1)) 'seat'
FROM
    pass_in_trip;

/* 6.6 БД «Аеропорт». Вивести дані для таблиці Trip з об’єднаними
значеннями двох стовпців: town_from та town_to, з
додатковими коментарями типу: 'from Rostov to Paris'.*/
SELECT 
    trip_no,
    ID_comp,
    plane,
    CONCAT('from ', town_from, ' to ', town_to) flight,
    CONCAT('виліт: ',
            time_out,
            ', приліт: ',
            time_in) 'date and time'
FROM
    trip;

/* 7.1 БД «Комп. фірма». Знайдіть принтери, що мають найвищу
ціну. Вивести: model, price.*/
SELECT 
    product.model, price
FROM
    product
        JOIN
    printer ON product.model = printer.model
WHERE
    price = (SELECT 
            MAX(price)
        FROM
            printer);

/* 7.2 БД «Комп. фірма». Знайдіть ноутбуки, швидкість яких є
меншою за швидкість будь-якого з ПК. Вивести: type, model,
speed.*/
SELECT 
    type, product.model, speed
FROM
    product
        JOIN
    laptop ON product.model = laptop.model
WHERE
    speed < (SELECT 
            MIN(speed)
        FROM
            pc);

/* 7.3 БД «Комп. фірма». Знайдіть виробників найдешевших
кольорових принтерів. Вивести: maker, price.*/
SELECT 
    maker, MIN(price), product.model
FROM
    product
        JOIN
    printer ON product.model = printer.model
WHERE
    color = 'y'
GROUP BY color;

/* 7.4 БД «Комп. фірма». Знайдіть виробників, що випускають по
крайній мірі дві різні моделі ПК. Вивести: maker, число
моделей. (Підказка: використовувати підзапити у якості
обчислювальних стовпців та операцію групування)*/
SELECT 
    maker, COUNT(type)
FROM
    product
WHERE
    type = 'PC'
GROUP BY maker
HAVING COUNT(type) >= 2;

/* 7.5 БД «Комп. фірма». Знайдіть середній розмір жорсткого диску
ПК (одне значення на всіх) тих виробників, які також
випускають і принтери. Вивести: середній розмір жорсткого диску.*/
SELECT 
    AVG(hd)
FROM
    product
        JOIN
    pc ON product.model = pc.model
WHERE
    maker IN (SELECT 
            maker
        FROM
            (SELECT 
                maker
            FROM
                product
            WHERE
                type = 'PC'
            GROUP BY maker) AS MakerPC
        WHERE
            MakerPC.maker IN (SELECT 
                    maker
                FROM
                    product
                WHERE
                    type = 'Laptop'
                GROUP BY maker));

/* 7.6 БД «Аеропорт». Визначіть кількість рейсів з міста 'London' для
кожної дати таблиці Pass_in_trip. Вивести: date, число рейсів.*/
SELECT 
    date AS 'дата',
    COUNT(pass_in_trip.trip_no) AS 'число рейсів'
FROM
    pass_in_trip
        JOIN
    trip ON pass_in_trip.trip_no = trip.trip_no
WHERE
    town_from = 'London'
GROUP BY date;

/* 7.7 БД «Аеропорт». Визначіть дні, коли було виконано
максимальну кількість рейсів до міста 'Moscow'. Вивести: date,
число рейсів.*/
SELECT 
    tmp.date AS 'дата',
    MAX(tmp.count) AS 'число рейсів'
FROM
    (SELECT 
        date, COUNT(pass_in_trip.trip_no) AS count
    FROM
        pass_in_trip
    JOIN trip ON pass_in_trip.trip_no = trip.trip_no
    WHERE
        town_to = 'Moscow'
    GROUP BY date) AS tmp;

/* 7.8 БД «Кораблі». Для кожної країни визначити рік, у якому було
спущено на воду максимальна кількість її кораблів. У випадку,
якщо виявиться декілька таких років, тоді взяти мінімальний з
них. Вивести: country, кількість кораблів, рік.*/
SELECT 
    tmp.country, MAX(tmp.count) 'ship count', tmp.launched
FROM
    (SELECT 
        country, launched, COUNT(ships.class) AS count
    FROM
        ships
    JOIN classes ON ships.class = classes.class
    GROUP BY country , launched
    ORDER BY country , count DESC , launched) AS tmp
GROUP BY tmp.country;

/* 7.9 БД «Кораблі». Вкажіть битви, у яких брало участь по крайній
мірі два корабля однієї і тієї ж країни (Вибір країни
здійснювати через таблицю Ships, а назви кораблів для таблиці
Outcomes, що відсутні у таблиці Ships, не брати до уваги).
Вивести: назву битви, країну, кількість кораблів.*/
SELECT 
    battle, country, COUNT(BattleShip.ship)
FROM
    (SELECT 
        name AS battle, ship
    FROM
        battles
    JOIN outcomes ON battles.name = outcomes.battle) AS BattleShip
        JOIN
    (SELECT 
        name AS ship, country
    FROM
        ships
    JOIN classes ON ships.class = classes.class) AS ShipClass ON BattleShip.ship = ShipClass.ship
GROUP BY country , battle
HAVING COUNT(BattleShip.ship) >= 2;

/* 8.1 БД «Комп. фірма». Для таблиці Product отримати підсумковий
набір у вигляді таблиці зі стовпцями maker, pc, laptop та printer,
в якій для кожного виробника необхідно вказати кількість
продукції, що ним випускається, тобто наявну загальну
кількість продукції у таблицях, відповідно, PC, Laptop та
Printer. (Підказка: використовувати підзапити у якості
обчислювальних стовпців)*/
SELECT 
    product.maker,
    pcs.qty AS PC,
    printers.qty AS Printer,
    laptops.qty AS Laptop
FROM
    product
        LEFT JOIN
    (SELECT 
        maker, COUNT(pc.model) AS qty
    FROM
        product
    JOIN pc ON product.model = pc.model
    GROUP BY maker
    ORDER BY maker) AS pcs ON product.maker = pcs.maker
        LEFT JOIN
    (SELECT 
        maker, COUNT(printer.model) AS qty
    FROM
        product
    JOIN printer ON product.model = printer.model
    GROUP BY maker
    ORDER BY maker) AS printers ON product.maker = printers.maker
        LEFT JOIN
    (SELECT 
        maker, COUNT(laptop.model) AS qty
    FROM
        product
    JOIN laptop ON product.model = laptop.model
    GROUP BY maker
    ORDER BY maker) AS laptops ON product.maker = laptops.maker
GROUP BY maker;

/* 8.2 БД «Комп. фірма». Для кожного виробника знайдіть середній
розмір екрану для ноутбуків, що ним випускається. Вивести:
maker, середній розмір екрану. (Підказка: використовувати
підзапити у якості обчислювальних стовпців)*/
SELECT 
    product.maker,
    lap.avg AS AverageScreenSize
FROM
    product
        LEFT JOIN
    (SELECT 
        maker, avg(cast(screen as double)) AS avg
    FROM
        product join laptop on product.model=laptop.model
    GROUP BY maker
    ORDER BY maker) AS lap ON product.maker = lap.maker
GROUP BY maker;

/* 8.3 БД «Комп. фірма». Знайдіть максимальну ціну ПК, що
випускаються кожним виробником. Вивести: maker,
максимальна ціна. (Підказка: використовувати підзапити у
якості обчислювальних стовпців)*/
SELECT 
    product.maker, PCprice.MAXprice AS MAXprice
FROM
    product
        LEFT JOIN
    (SELECT 
        maker, MAX(price) AS MAXprice
    FROM
        product
    JOIN pc ON product.model = pc.model
    GROUP BY maker
    ORDER BY maker) AS PCprice ON product.maker = PCprice.maker
GROUP BY maker
ORDER BY maker;

/* 8.4 БД «Комп. фірма». Знайдіть мінімальну ціну ПК, що
випускаються кожним виробником. Вивести: maker,
максимальна ціна. (Підказка: використовувати підзапити у
якості обчислювальних стовпців)*/
SELECT 
    product.maker, PCprice.MINprice AS MINprice
FROM
    product
        LEFT JOIN
    (SELECT 
        maker, MIN(price) AS MINprice
    FROM
        product
    JOIN pc ON product.model = pc.model
    GROUP BY maker
    ORDER BY maker) AS PCprice ON product.maker = PCprice.maker
GROUP BY maker
ORDER BY maker;

/* 8.5 БД «Комп. фірма». Для кожного значення швидкості ПК, що
перевищує 600 МГц, визначіть середню ціну ПК з такою ж
швидкістю. Вивести: speed, середня ціна. (Підказка:
використовувати підзапити у якості обчислювальних стовпців)*/
SELECT 
    speed, AVG(price) AS AVGprice
FROM
    pc
WHERE
    speed >= 600
GROUP BY speed;

/* 8.6 БД «Комп. фірма». Знайдіть середній розмір жорсткого диску
ПК кожного з тих виробників, які випускають також і
принтери. Вивести: maker, середній розмір жорсткого диску.
(Підказка: використовувати підзапити у якості
обчислювальних стовпців)*/
SELECT 
    maker, AVG(hd)
FROM
    pc
        JOIN
    product ON pc.model = product.model
WHERE
    maker IN (SELECT 
            maker
        FROM
            (SELECT 
                maker
            FROM
                product
            WHERE
                type = 'PC'
            GROUP BY maker) AS MakerPC
        WHERE
            MakerPC.maker IN (SELECT 
                    maker
                FROM
                    product
                WHERE
                    type = 'Printer'
                GROUP BY maker))
GROUP BY maker;

/* 8.7 БД «Кораблі». Вкажіть назву, водотоннажність та число
гармат кораблів, що брали участь у битві при 'Guadalcanal'.
Вивести: ship, displacement, numGuns. (Підказка:
використовувати підзапити у якості обчислювальних стовпців)*/
SELECT 
    name AS ship, displacement, numGuns
FROM
    ships
        JOIN
    classes ON ships.class = classes.class
WHERE
    name IN (SELECT 
            ship
        FROM
            outcomes
        WHERE
            battle = 'Guadalcanal');

/* 8.8 БД «Кораблі». Вкажіть назву, країну та число гармат кораблів,
що були пошкоджені у битвах. Вивести: ship, country, numGuns.
(Підказка: використовувати підзапити у якості обчислювальних стовпців)*/
SELECT 
    name AS ship, country, numGuns
FROM
    ships
        JOIN
    classes ON ships.class = classes.class
WHERE
    name IN (SELECT 
            ship
        FROM
            outcomes
        WHERE
            result = 'damaged');

/* 9.1 БД «Комп. фірма». Для таблиці Product отримати підсумковий
набір у вигляді таблиці зі стовпцями maker, pc, в якій для
кожного виробника необхідно вказати, чи виробляє він ('yes'),
чи ні ('no') відповідний тип продукції. У першому випадку
('yes') додатково вказати поруч у круглих дужках загальну
кількість наявної (тобто, що знаходиться у таблиці PC)
продукції, наприклад, 'yes(2)'.*/
SELECT 
    product.maker,
    CASE
        WHEN pcs.qty IS NULL THEN 'No'
        ELSE CONCAT('Yes(', pcs.qty, ')')
    END AS PC
FROM
    product
        LEFT JOIN
    (SELECT 
        maker, COUNT(pc.model) AS qty
    FROM
        product
    JOIN pc ON product.model = pc.model
    GROUP BY maker
    ORDER BY maker) AS pcs ON product.maker = pcs.maker
GROUP BY maker;

/* 9.2 БД «Фірма прий. вторсировини». Приймаючи, що прихід та
розхід грошей на кожному пункті прийому фіксується не
частіше одного разу на день (лише таблиці Income_o та
Outcome_o), написати запит з такими вихідними даними: point
(пункт), date (дата), inc (прихід), out (розхід). (Підказка:
використовувати зовнішнє з’єднання та оператор CASE)*/
SELECT 
    income_o.point,
    income_o.date,
    inc,
    CASE
        WHEN outcome_o.out IS NULL THEN '0'
        ELSE outcome_o.out
    END AS 'out'
FROM
    income_o
        LEFT OUTER JOIN
    outcome_o ON income_o.point = outcome_o.point
        AND income_o.date = outcome_o.date;

/* 9.3 БД «Кораблі». Визначити назви усіх кораблів з таблиці Ships,
які задовольняють, у крайньому випадку, комбінації будь-яких
чотирьох критеріїв з наступного списку: numGuns=8, bore=15,
displacement=32000, type='bb', country='USA', launched=1915,
class='Kongo'. Вивести: name, numGuns, bore, displacement,
type, country, launched, class.*/
SELECT 
    t.name
FROM
    (SELECT 
        ships.name,
            CASE
                WHEN classes.numGuns = 8 THEN 1
                ELSE 0
            END AS c1,
            CASE
                WHEN classes.bore = 15 THEN 1
                ELSE 0
            END AS c2,
            CASE
                WHEN classes.displacement = 32000 THEN 1
                ELSE 0
            END AS c3,
            CASE
                WHEN classes.type = 'bb' THEN 1
                ELSE 0
            END AS c4,
            CASE
                WHEN ships.launched = 1915 THEN 1
                ELSE 0
            END AS c5,
            CASE
                WHEN classes.class = 'Kongo' THEN 1
                ELSE 0
            END AS c6,
            CASE
                WHEN classes.country = 'USA' THEN 1
                ELSE 0
            END AS c7
    FROM
        ships
    INNER JOIN classes ON ships.class = classes.class) t
WHERE
    (t.c1 + t.c2 + t.c3 + t.c4 + t.c5 + t.c6 + t.c7) >= 4;

/* 9.4 БД «Аеропорт». Для кожного рейсу (таблиця Trip) визначити
тривалість його польоту. Вивести: trip_no, назва компанії,
plane, town_from, town_to, тривалість польоту. (Підказка:
використати для перевірки умов оператор CASE)*/
SELECT 
    trip_no,
    name,
    plane,
    town_from,
    town_to,
    TIMEDIFF(time_in, time_out) AS 'тривалість польоту'
FROM
    trip
        JOIN
    company ON trip.ID_comp = company.ID_comp;

/* 10.1 БД «Комп. фірма». Знайдіть номера моделей та ціни усіх
продуктів (будь-якого типу), що випущені виробником 'B'.
Вивести: maker, model, type, price. (Підказка: використовувати
оператор UNION)*/
SELECT 
    tmp.model, price, type
FROM
    (SELECT 
        model, price
    FROM
        PC UNION SELECT 
        model, price
    FROM
        Laptop UNION SELECT 
        model, price
    FROM
        printer) AS tmp
        JOIN
    product ON tmp.model = product.model
WHERE
    tmp.model IN (SELECT 
            model
        FROM
            product
        WHERE
            maker = 'B')
ORDER BY price DESC;

/* 10.2 БД «Комп. фірма». Для кожної моделі продукції з усієї БД
виведіть її найвищу ціну. Вивести: type, model, максимальна
ціна. (Підказка: використовувати оператор UNION)*/
SELECT 
    tmp.model, type, tmp.max AS MAXprice
FROM
    (SELECT 
        model, MAX(price) AS max
    FROM
        PC
    GROUP BY model UNION SELECT 
        model, MAX(price) AS max
    FROM
        Laptop
    GROUP BY model UNION SELECT 
        model, MAX(price) AS max
    FROM
        printer
    GROUP BY model) AS tmp
        JOIN
    product ON tmp.model = product.model
ORDER BY tmp.max;

/* 10.3 БД «Комп. фірма». Знайдіть середню ціну ПК та ноутбуків, що
випущені виробником 'A'. Вивести: одна загальна середня ціна.
(Підказка: використовувати оператор UNION)*/
SELECT 
    AVG(price)
FROM
    (SELECT 
        model, price
    FROM
        PC UNION SELECT 
        model, price
    FROM
        Laptop) AS tmp
        JOIN
    product ON tmp.model = product.model
WHERE
    tmp.model IN (SELECT 
            model
        FROM
            product
        WHERE
            maker = 'A');

/* 10.4 БД «Кораблі». Перерахуйте назви головних кораблів, що є
наявними у БД (врахувати також і кораблі з таблиці
Outcomes). Вивести: назва корабля, class. (Підказка:
використовувати оператор UNION та операцію EXISTS)*/
SELECT 
    tmp.name, class
FROM
    ((SELECT 
        name
    FROM
        ships
    WHERE
        name = class) UNION (SELECT 
        name
    FROM
        ships
    WHERE
        name IN (SELECT 
                ship
            FROM
                outcomes))) AS tmp
        JOIN
    ships ON tmp.name = ships.name;

/* 10.5 БД «Кораблі». Знайдіть класи, у яких входить лише один
корабель з усієї БД (врахувати також кораблі у таблиці
Outcomes, яких немає у таблиці Ships). Вивести: class.*/
SELECT 
    class
FROM
    ((SELECT 
        name
    FROM
        ships
    GROUP BY class
    HAVING COUNT(name) = 1) UNION (SELECT 
        class
    FROM
        outcomes
    JOIN ships ON outcomes.ship = ships.name)) AS tmp
        JOIN
    ships ON tmp.name = ships.name
WHERE
    EXISTS( SELECT 
            name
        FROM
            ships
        GROUP BY class
        HAVING COUNT(name) = 1);

/* 10.6 БД «Кораблі». Для кожного класу порахувати кількість
кораблів, що входить до нього (врахувати також кораблі у
таблиці Outcomes, яких немає у таблиці Ships). Вивести: class,
кількість кораблів у класі.*/
SELECT 
    class,
    COUNT(tmp.name) AS 'кількість кораблів класy'
FROM
    (SELECT 
        name AS name
    FROM
        ships UNION ALL SELECT 
        ship AS name
    FROM
        outcomes) AS tmp
        JOIN
    ships ON tmp.name = ships.name
GROUP BY class;

/* 10.7 БД «Кораблі». Знайдіть класи, в які входять лише один або два
кораблі з цілої БД (врахувати також кораблі у таблиці
Outcomes, яких немає у таблиці Ships). Вивести: class,
кількість кораблів у класі.*/
SELECT 
    class,
    COUNT(tmp.name) AS 'кількість кораблів класy'
FROM
    (SELECT 
        name AS name
    FROM
        ships UNION ALL SELECT 
        ship AS name
    FROM
        outcomes) AS tmp
        JOIN
    ships ON tmp.name = ships.name
GROUP BY class
HAVING COUNT(tmp.name) <= 2;

/* 10.8 БД «Кораблі». Знайдіть назви усіх кораблів з БД, про які
можна однозначно сказати, що вони були спущені на воду до
1942 р. Вивести: назву кораблів.*/
SELECT DISTINCT
    tmp.name, launched
FROM
    (SELECT 
        name AS name
    FROM
        ships UNION ALL SELECT 
        ship AS name
    FROM
        outcomes) AS tmp
        JOIN
    ships ON tmp.name = ships.name
WHERE
    launched < 1942
ORDER BY tmp.name;