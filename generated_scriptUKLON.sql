-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema Uklondb
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema Uklondb
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `Uklondb` DEFAULT CHARACTER SET utf8 ;
USE `Uklondb` ;

-- -----------------------------------------------------
-- Table `Uklondb`.`user`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Uklondb`.`user` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` NVARCHAR(20) NOT NULL,
  `surname` NVARCHAR(20) NOT NULL,
  `email` NVARCHAR(45) NOT NULL,
  `password` NVARCHAR(45) NOT NULL,
  `phone` NVARCHAR(15) NOT NULL,
  `rate` DOUBLE NULL DEFAULT 0.0,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC) VISIBLE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Uklondb`.`driver`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Uklondb`.`driver` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` NVARCHAR(20) NOT NULL,
  `surname` NVARCHAR(20) NOT NULL,
  `email` NVARCHAR(45) NOT NULL,
  `pasword` NVARCHAR(45) NOT NULL,
  `phone` NVARCHAR(15) NOT NULL,
  `rate` DOUBLE NULL DEFAULT 0.0,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC) VISIBLE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Uklondb`.`car`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Uklondb`.`car` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `model` NVARCHAR(20) NOT NULL,
  `number` NVARCHAR(20) NOT NULL,
  `color` NVARCHAR(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC) VISIBLE,
  UNIQUE INDEX `number_UNIQUE` (`number` ASC) VISIBLE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Uklondb`.`location`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Uklondb`.`location` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `longitude` DOUBLE NOT NULL,
  `latitude` DOUBLE NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Uklondb`.`order`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Uklondb`.`order` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `driver_rate` DOUBLE NULL,
  `distance` DOUBLE NULL DEFAULT 0,
  `datetime` DATETIME NOT NULL,
  `price` DECIMAL(13,2) NOT NULL,
  `user_id` INT NOT NULL,
  `driver_id` INT NOT NULL,
  `departure_location_id` INT NOT NULL,
  `destination_location_id` INT NOT NULL,
  `car_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_order_user1_idx` (`user_id` ASC) VISIBLE,
  INDEX `fk_order_driver1_idx` (`driver_id` ASC) VISIBLE,
  INDEX `fk_order_location1_idx` (`departure_location_id` ASC) VISIBLE,
  INDEX `fk_order_location2_idx` (`destination_location_id` ASC) VISIBLE,
  INDEX `fk_order_car1_idx` (`car_id` ASC) VISIBLE,
  CONSTRAINT `fk_order_user1`
    FOREIGN KEY (`user_id`)
    REFERENCES `Uklondb`.`user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE CASCADE,
  CONSTRAINT `fk_order_driver1`
    FOREIGN KEY (`driver_id`)
    REFERENCES `Uklondb`.`driver` (`id`)
    ON DELETE NO ACTION
    ON UPDATE CASCADE,
  CONSTRAINT `fk_order_location1`
    FOREIGN KEY (`departure_location_id`)
    REFERENCES `Uklondb`.`location` (`id`)
    ON DELETE NO ACTION
    ON UPDATE CASCADE,
  CONSTRAINT `fk_order_location2`
    FOREIGN KEY (`destination_location_id`)
    REFERENCES `Uklondb`.`location` (`id`)
    ON DELETE NO ACTION
    ON UPDATE CASCADE,
  CONSTRAINT `fk_order_car1`
    FOREIGN KEY (`car_id`)
    REFERENCES `Uklondb`.`car` (`id`)
    ON DELETE NO ACTION
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Uklondb`.`favorite_place`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Uklondb`.`favorite_place` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` NVARCHAR(20) NOT NULL,
  `user_id` INT NOT NULL,
  `location_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_location_user1_idx` (`user_id` ASC) VISIBLE,
  INDEX `fk_favorite_place_location1_idx` (`location_id` ASC) VISIBLE,
  CONSTRAINT `fk_location_user1`
    FOREIGN KEY (`user_id`)
    REFERENCES `Uklondb`.`user` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_favorite_place_location1`
    FOREIGN KEY (`location_id`)
    REFERENCES `Uklondb`.`location` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Uklondb`.`driver_has_car`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Uklondb`.`driver_has_car` (
  `driver_id` INT NOT NULL,
  `car_id` INT NOT NULL,
  PRIMARY KEY (`driver_id`, `car_id`),
  INDEX `fk_driver_has_car_car1_idx` (`car_id` ASC) VISIBLE,
  INDEX `fk_driver_has_car_driver1_idx` (`driver_id` ASC) VISIBLE,
  CONSTRAINT `fk_driver_has_car_driver1`
    FOREIGN KEY (`driver_id`)
    REFERENCES `Uklondb`.`driver` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_driver_has_car_car1`
    FOREIGN KEY (`car_id`)
    REFERENCES `Uklondb`.`car` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
