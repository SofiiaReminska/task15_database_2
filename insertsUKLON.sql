INSERT INTO uklondb.user(user.name, surname, email, password, phone) VALUES 
('Sofiia', 'Reminska', 'sofii.remi@gmail.com', 'qwerty', '0938988989'),
('Volodia', 'Voloshun', 'volodia.volo1@gmail.com', 'qwerty', '0938912389'),
('Volodia', 'Chuk', 'volodia.chuk99@gmail.com', 'sifgho', '0678912389'),
('Ivan', 'Ivanow', 'ivan.ivan101@gmail.com', 'slnfkn', '0989912389'),
('Ivan', 'Dubar', 'ivan.dubarrr@gmail.com', 'sodj', '0930912389'),
('Petro', 'Pinch', 'perro3434@gmail.com', 'asdfg', '0678912389'),
('Sergio', 'Suntin', 'ser.volodar@gmail.com', 'qweraklc', '0938923495'),
('Yura', 'Hard', 'yure.yura123@gmail.com', 'sunflower', '0983453452'),
('Oleh', 'Biluk', 'oleh.bigboss@gmail.com', 'qwerty', '0991234567'),
('Volodia', 'Voloshun', 'volodia.volo1@gmail.com', 'rgureioero', '0671239045');

INSERT INTO uklondb.driver(driver.name, surname, email, driver.pasword, phone) VALUES 
('Volodia', 'Velukuj', 'volodia.volodia@gmail.com', 'qwerty', '0508912389'),
('Volodymyr', 'Tkach', 'volodia.chacha567@gmail.com', '123690kjh', '0508912389'),
('Volodia', 'Bass', 'volodia.bass2bass@gmail.com', 'prfjcmkps', '0508912389'),
('Ivan', 'Hrom', 'ivan.ivanko222@gmail.com', 'slnfkn', '0938994324'),
('Ivan', 'Kolesov', 'ivan.koll00as0@gmail.com', 'aowimx', '0500912389'),
('Petro', 'Kytskui', 'perro3496@gmail.com', 'qeipdmck790xm', '0679012389'),
('Svitlana', 'Chorna', 'svitlanka234@gmail.com', 'qwerty', '0674567110'),
('Yura', 'Dobruy', 'yure.yur@gmail.com', 'asdfgh2345', '0671153452'),
('Andriy', 'Dorobank', 'andrrr.2929@gmail.com', 'qwerty', '0546734567'),
('Anton', 'Antinskui', 'antant123@gmail.com', 'rgureioero', '0671239045');

INSERT INTO uklondb.car(model, number, color) VALUES 
('Nissan', 'ВС2345ВВ', 'black'),
('Volkswagen', 'ВС5567ВС', 'white'),
('Nissan', 'АА2345ВC', 'green'),
('Volkswagen', 'ВС2255ВІ', 'blue'),
('Opel', 'ВС1234ВВ', 'grey'),
('Lanos', 'ВС9090ВВ', 'black'),
('Lanos', 'АІ2345ВІ', 'white'),
('Mercedes', 'ВС0911ВВ', 'white'),
('Mercedes', 'АА2345ВВ', 'black'),
('Nissan', 'ВС4747ВС', 'grey');

INSERT INTO uklondb.driver_has_car(driver_id, car_id) VALUES 
('1', '2'),
('2', '7'),
('3', '1'),
('4', '4'),
('5', '10'),
('6', '5'),
('7', '9'),
('8', '8'),
('9', '3'),
('10', '6');

INSERT INTO uklondb.location(longitude, latitude) VALUES 
('49.857958', '24.035487'),
('49.837319', '24.025000'),
('49.841490', '24.004506'),
('49.842376', '24.003776'),
('49.871675', '24.054649'),
('49.869272', '24.030176'),
('49.814428', '23.997642'),
('49.815667', '24.007598'),
('49.812337', '24.043367'),
('49.809911', '24.055990');

INSERT INTO uklondb.favorite_place(name, user_id, location_id) VALUES 
('home', '1', '1'),
('home', '2', '2'),
('home', '3', '3'),
('work', '1', '4'),
('work', '5', '5'),
('work', '6', '6'),
('study', '7', '7'),
('study', '9', '8'),
('home', '4', '9'),
('home', '5', '10');

INSERT INTO uklondb.order(driver_rate, distance, datetime, price, user_id, driver_id, departure_location_id, destination_location_id, car_id) VALUES 
('3','7','2019-03-22 00:00:00.000','88','1','2','1','3','3'),
('4','8','2019-03-23 00:00:00.000','105','1','9','3','5','5'),
('4','10','2019-04-29 00:00:00.000','99','1','7','4','1','3'),
('5','12','2019-04-08 00:00:00.000','70','3','2','5','2','3'),
('3','7','2019-04-13 00:00:00.000','87','3','5','6','5','5'),
('1','6','2019-04-14 00:00:00.000','55','4','10','7','8','1'),
('5','9','2019-11-06 00:00:00.000','103','5','4','8','9','1'),
('5','4','2019-11-09 00:00:00.000','75','5','7','9','10','1'),
('4','5','2019-11-29 00:00:00.000','81','6','1','10','1','4'),
('3','3','2019-11-14 00:00:00.000','115','9','9','1','3','6');